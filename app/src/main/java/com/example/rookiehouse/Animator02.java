package com.example.rookiehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Animator02 extends AppCompatActivity {
    private Button btn;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animator02);
        btn=findViewById(R.id.btn);
        textView=findViewById(R.id.text);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //1.旋转
//                ObjectAnimator animator = ObjectAnimator.ofFloat(textView, "rotation", 0f, 360f);
//                animator.setDuration(5000);
//                animator.start();
                //2.位移，移出去，再回来
//                float curTranslationX = textView.getTranslationX();
////                ObjectAnimator animator = ObjectAnimator.ofFloat(textView, "translationX", curTranslationX, -500f, curTranslationX);
////                animator.setDuration(5000);
////                animator.start();


//                3.缩放
//                ObjectAnimator animator = ObjectAnimator.ofFloat(textView, "scaleY", 1f, 3f, 1f);
//                animator.setDuration(5000);
//                animator.start();
//               4. 组合动画
                ObjectAnimator moveIn = ObjectAnimator.ofFloat(textView, "translationX", -500f, 0f);
                ObjectAnimator rotate = ObjectAnimator.ofFloat(textView, "rotation", 0f, 360f);
                ObjectAnimator fadeInOut = ObjectAnimator.ofFloat(textView, "alpha", 1f, 0f, 1f);
                AnimatorSet animSet = new AnimatorSet();
                animSet.play(rotate).with(fadeInOut).after(moveIn);
                animSet.setDuration(5000);
                moveIn.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Toast.makeText(getApplicationContext(),"结束了",Toast.LENGTH_LONG);
                    }
                });
                animSet.start();
            }
        });
    }
}
