package com.example.rookiehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;

public class Animation04 extends AppCompatActivity {
    private LinearLayout start_ctrl;
    private Button showBtn;
    private  Button hideBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation04);
        showBtn=findViewById(R.id.showLogin);
        hideBtn=findViewById(R.id.hidelogin);

        start_ctrl = (LinearLayout) findViewById(R.id.start_ctrl);
        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //设置动画，从自身位置的最下端向上滑动了自身的高度，持续时间为500ms
                final TranslateAnimation ctrlAnimation = new TranslateAnimation(
                        TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                        TranslateAnimation.RELATIVE_TO_SELF, 1, TranslateAnimation.RELATIVE_TO_SELF, 0);
                ctrlAnimation.setDuration(500l);     //设置动画的过渡时间
                start_ctrl.setVisibility(View.VISIBLE);
                start_ctrl.startAnimation(ctrlAnimation);
//                start_ctrl.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        start_ctrl.setVisibility(View.VISIBLE);
//                        start_ctrl.startAnimation(ctrlAnimation);
//                    }
//                }, 2000);
            }
        });
        hideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //设置动画，从自身位置的最下端向上滑动了自身的高度，持续时间为500ms
                final TranslateAnimation ctrlAnimation = new TranslateAnimation(
                        TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                        TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 1);
                ctrlAnimation.setDuration(500l);     //设置动画的过渡时间
               // start_ctrl.setVisibility(View.);
                start_ctrl.startAnimation(ctrlAnimation);
                start_ctrl.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            start_ctrl.setVisibility(View.GONE);
                        }
                    }, 500);
                }
        });


    }
}
